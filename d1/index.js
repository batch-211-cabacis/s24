// console.log("Hello World!");

// ES6 Updates
/*
	- one of the lates version of writing JS and in fact one of the major update to JS

	- let, const - are ES6 update, these are the new standards of creating variables
*/

// Exponent operator

const firstNum = 8 ** 2;
console.log(firstNum); //64 -- ES6 update

const secondName = Math.pow(8,2);
console.log(secondName); // used before the update


let string1 = "fun";
let string2 = "Bootcamp";
let string3 = "Coding";
let string4 = "JavaScript";
let string5 = "Zuitt";
let string6 = "Learning";
let string7 = "love"
let string8 = "I";
let string9 = "is";
let string10 = "in";


// Mini activity 1

// let sentence1 = string8.concat(" ",string7," ",string6," ",string4);
// console.log(sentence1);
// let sentence2 = string3.concat(string2)

// solution
let sentence1 = string8 + " " + string7 + " " + string5 + " " + string3; + string2 + "!";
console.log(sentence1);
let sentence2 = string6 + " " + string4 + " " + string9 + " " + string1; + "!";
console.log(sentence2);

// string literals "" (double quote), `` (backticks) , '' (single quote)

// Template Literals
/*
	- this allow us to create strings using `` and easily embed JS expression in it
	- it allows us to write string without using the concatenation operator (+)
	- greatly helps with code readability
*/

let sentence3 = `${string8} ${string7} ${string5} ${string3} ${string2}!`;
console.log(sentence3);

/*
	${} is a placeholder that is used to embed JS expressions when creating strings using Template literals
*/

let name = "John";

// Pre-Template Literal String
// ("") or ('')

let message = "Hello" + name + "! Welcome to programming!";
message = `Hello ${name} ! Welcome to programming!`
console.log(message);

// Strings using template literals
// (``) backticks

// Multi-line using Template Literals

const anotherMessage = `
${name} attended a Math competition. 
He won it by solving the problem 8 ** 2 with solution of ${firstNum}.

`
console.log(anotherMessage);

let dev = {
	name: "Peter",
	lastName: "Parker",
	occupation: "web developer",
	income: 50000,
	expenses: 60000
};
console.log(`${dev.name} is a ${dev.occupation}`);
console.log(`His income is ${dev.income} and expenses at ${dev.expenses}. His current balance is ${dev.income - dev.expenses}.`);

const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savings account is ${principal * interestRate}`);


// Array Destructuring 
/*
	- allows us to unpack elemnts arrays into distinct variables

	SYNTAX
		let/const [variableNameA, variableNameB, variableNameC] = array;
*/

const fullName = ["Juan", "Dela", "Cruz"];

// Pre-array destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It is nice to meet you!`);

// Array destructuring (currently used)

const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It is nice to meet you!`);

// Object Destructuring
/*
	- allows us to unpack properties of objects into distinct variables
	- it shortens the syntax

	SYNTAX
		let/const {propertyNameA, propertyNameB, propertyNameC} = object
*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
};

// Pre-object destructuring
// we can access them using . or []

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

// Object destructuring

const {givenName, maidenName, familyName} = person

// person = {givenName, maidenName, familyName} // will give error
console.log(givenName);
console.log(maidenName);
console.log(familyName);

// Arrow Functions
/*
	- compact alternative syntax to traditional functions
*/

const hello = () => {
	console.log("Hello World");
};
hello(); // this is an lternative syntax -- => is called arrow function 

// const hello = function hello(){
// 	console.log("Hello World");
// };

// hello();

// Pre-arrow function and template literals
/*
	SYNTAX
	function functionName(paramA, paramB, paramC){
	console.log();
	}
*/

// Arrow function
/*
	SYNTAX
	let/const variableName = (paramA, paramB, paramC) => {
	console.log;
	}
*/

const printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`)
}
printFullName("John", "D", "Smith");

// Arrow functions with loops

// Pre-arrow function

const students = ["John", "Jane", "Judy"];

// forEach is used for iteration in array, also it loops through each elements

students.forEach(function(student){
	console.log(`${student} is a student`);
});

// Arrow function

students.forEach((student) =>{
	console.log(`${student} is a student`)
});

// students.forEach((student)=>console.log(student "is a student"));


// Implicit return statement
/*
	- there are instances when you can omit the "return" statement
	- this works because even without the "return" statement JS IMPLICITLY adds it for the result of the function
*/

// Pre-arrow function

// const add = (x,y) => {
// 	return x + y;
// }
// let total = add(1,2);
// console.log(total);

// Arrow function 
	// {} in an arrow function are code blocks. if an arrow function has a {} or code block you have to use a "return"

	// implicit return will only work on arrow functions without {}

const add = (x,y) => x + y;
let total = add(1,2);
console.log(total);

// Mini activity 2

const subtract = (x,y) => x - y;
let total1 = subtract(1,2);
console.log(total1);

const multiply = (x,y) => x * y;
let total2 = multiply(1,2);
console.log(total2);

const divide = (x,y) => x / y;
let total3 = divide(1,2);
console.log(total3);


// Default function argument value
/*
	- provide a default argument value if non is provided when the function is invoke
*/

const greet = (name = 'User') =>{
	return `Good evening, ${name}`;
}
console.log(greet());
console.log(greet("John"));

// Class-Based Object Blueprint
/*
	- allows creation/instantiation of objects using classes as blueprints
*/

// Creating a class

/*
	- the constructor is a special method of a class for creating/initializing an object for that class

	SYNTAX
		class className {
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertA = objectPropertyA;
				this.objectPropertB = objectPropertyB;
			}
		}
*/
		// using "this"  allows to access the property
		class Car {
			constructor(brand, name, year){
				this.brand = brand;
				this.name = name;
				this.year = year;
			}
		}

		// Instantiate an Object
		/*
			- the "new" operator creates/instantiates a new object with the given arguments as the value of its properties
		*/

		 // let/const variableName = new className(); // if there is no object it will be undefined

		 /*
			- creating a constant with the "const" keyword and assigning it a value of an object makes it so we can't reassign it with another data type
		 */

		 const myCar = new Car();

		 console.log(myCar);

		 myCar.brand = "Ford";
		 myCar.name = "Everest";
		 myCar.year = "1996";

		 console.log(myCar);

		 const myNewCar = new Car("Toyota", "Vios", "2021");
		 console.log(myNewCar);

		 // Mini activity 3

		 class Character {
			constructor(name, role, strength, weakness){
				this.name = name;
				this.role = role;
				this.strength = strength;
				this.weakness = weakness;
			}
		}

		const myCharacter = new Character("Pikachu", "Pokemon", "Banana", "Orange");
		console.log(myCharacter);

		class Character2 {
			constructor(name, role, strength, weakness){
				this.name = name;
				this.role = role;
				this.strength = strength;
				this.weakness = weakness;
			}
		}

		const myCharacter2 = new Character2("Eevee", "Pokemon", "Apple", "Mango");
		console.log(myCharacter2);

